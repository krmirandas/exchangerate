import { Component, OnInit } from '@angular/core';
import { Exchangerate } from 'src/app/_models/exchangerate';
import { ExchangerateService } from 'src/app/_services/exchangerate.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  exchangerate: Exchangerate | any = {};
  currency: String = 'usd';

  constructor(private api: ExchangerateService) { 
  }

  ngOnInit(): void {
    this.getRates();
  }

  getRates() {
    this.api.getRates(this.currency).subscribe(
      res => {
        this.exchangerate = res;
      }, err => console.log(err)
    )
  }


}
