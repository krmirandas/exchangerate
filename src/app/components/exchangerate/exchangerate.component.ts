import { Component, OnInit } from '@angular/core';
import { Exchangerate } from 'src/app/_models/exchangerate';
import { ExchangerateService } from 'src/app/_services/exchangerate.service';

@Component({
  selector: 'app-exchangerate',
  templateUrl: './exchangerate.component.html',
  styleUrls: ['./exchangerate.component.css']
})
export class ExchangerateComponent implements OnInit {

  exchangerate: Exchangerate | any;
  currency: String = 'usd';
  currencies: any = [];
  currencySelected: String = '';
  rates: any = [];

  constructor(private api: ExchangerateService) { }

  ngOnInit(): void {
    this.rates = [];
    this.getRates();
  }

  getRates() {
    this.api.getRates(this.currency).subscribe(
      res => {
        this.exchangerate = res;

        for (const curr of Object.keys(this.exchangerate.rates)) {
          this.currencies.push(curr);
        }
      }, err => console.log(err)
    )
  }

  capturar() {
    this.ngOnInit();
    // Pasamos el valor seleccionado a la variable currencySelected
    this.api.getRates(this.currencySelected).subscribe(
      res => {
        this.exchangerate = res;
        
        for (const curr of Object.keys(this.exchangerate.rates)) {
          this.rates.push({
            currency: curr,
            rate: this.exchangerate.rates[curr]
          })
        }

        
      }, err => console.log(err)
    )
  }
}
