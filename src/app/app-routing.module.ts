import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExchangerateComponent } from './components/exchangerate/exchangerate.component';

const routes: Routes = [
  { path: '', component: ExchangerateComponent },
  { path: 'exchangerate', component: ExchangerateComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
