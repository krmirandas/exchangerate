export class Exchangerate {
    base: string;
    date: string;
    time_last_updated: number;
    rates: Array<any>;

    constructor(base, date, time_last_updated, rates) {
        this.base = base;
        this.date = date;
        this.time_last_updated = time_last_updated;
        this.rates = rates;
    }
}