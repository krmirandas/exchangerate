import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExchangerateService {

  API = 'https://api.exchangerate-api.com/';

  constructor(private http: HttpClient) { }

  getRates(currency) {
    return this.http.get(this.API + 'v4/latest/' + currency);
  }
}
